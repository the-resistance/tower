FROM node:20-alpine

MAINTAINER Resistance Research

RUN apk update && \
    apk add --no-cache \
    git

WORKDIR /one

COPY package*.json server.js client.js ./

RUN npm install --omit=dev

RUN chown -R node /one && chmod -R 750 /one
RUN mkdir -p /gun && chown -R node /gun && chmod -R 740 /gun

EXPOSE 8080

USER node

ENV NODE_ENV=production

CMD ["node", "server.js"]